package com.krishlabs.walkmore;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.firebase.ui.auth.AuthUI;


public class Dashboard extends AppCompatActivity {
    private Button logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        logout = (Button)this.findViewById(R.id.btnLogout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOut();
                startActivity(new Intent((Context) com.krishlabs.walkmore.Dashboard.this, MainActivity.class));
            }});
    }

    public final void logOut() {
        AuthUI.getInstance().signOut((Context)this);
    }
}
