package com.krishlabs.walkmore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseAuth.getInstance().addAuthStateListener((FirebaseAuth.AuthStateListener) (new FirebaseAuth.AuthStateListener() {
            public final void onAuthStateChanged( FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    startActivity(new Intent((Context) com.krishlabs.walkmore.MainActivity.this, UpdateProfile.class));
                } else {
                    signIn();
                }

            }
        }));
    }

    private final void signIn(){
        List providers = Arrays.asList(new AuthUI.IdpConfig[]{(new AuthUI.IdpConfig.GoogleBuilder()).build(), (new AuthUI.IdpConfig.EmailBuilder()).build(), (new AuthUI.IdpConfig.PhoneBuilder()).build(), (new AuthUI.IdpConfig.FacebookBuilder()).build()});
        Intent authIntent = (((((AuthUI.SignInIntentBuilder)AuthUI.getInstance().createSignInIntentBuilder()).setAvailableProviders(providers)).setIsSmartLockEnabled(false)).setTosAndPrivacyPolicyUrls("example.termsofservice.com", "example.privatepolicy.com")).build();
        this.startActivity(authIntent);
    }

}